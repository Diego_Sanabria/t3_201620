package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;



import java.util.Iterator;

import estructuras.Cola;
import estructuras.Pila;
import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;
	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}
	
	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
			+ "1. Registrar Cliente \n"
			+ "2. Parquear Siguente Carro En Cola \n"
			+ "3. Atender Cliente Que Sale \n"
	        + "4. Ver Estado Parqueaderos \n"
	        + "5. Ver Carros En Cola \n"
			+ "6. Salir \n\n"
			+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			int op1 = Integer.parseInt(br.readLine());
			while (op1>6 || op1<1)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");
				System.out.print(mensaje);
				op1 = Integer.parseInt(br.readLine());
			}

			switch(op1)
			{
				case 1: registrarCliente(); break;
				case 2: parquearCarro(); break;
				case 3: salidaCliente(); break;
				case 4: verEstadoParquederos(); break;
				case 5: verCarrosEnCola(); break;
				case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
			}
		}

	}

  /**
   * M�todo para registrar a un nuevo cliente
   * @throws Exception
   */
	public void registrarCliente () throws Exception
  {
	  System.out.println("** REGISTRAR CLIENTE **");
	  System.out.println("Ingrese la matr�cula del veh�culo: ");
	  String matricula = br.readLine();
	  System.out.println("Ingrese el color del veh�culo: ");
	  String color = br.readLine();
	  System.out.println("Ingrese el nombre del conductor: ");
	  String nombre = br.readLine();
	  
	  central.registrarCliente(color, matricula, nombre);
	  
	  System.out.println("Cliente registrado con �xito");
	  System.out.println("NOMBRE: "+nombre);
	  System.out.println("MATRICULA: "+matricula);
	  System.out.println("COLOR: " + color);
	  
  }
  /**
   * M�todo para parquear un carro que se encuentra en la cola
   * @throws Exception
   */
  public void parquearCarro() throws Exception
  {
	  System.out.println("** PARQUEAR CARRO EN COLA **");
	  try{
		  System.out.println(central.parquearCarroEnCola());
	  }catch(Exception e){
		  throw new Exception("Imposible parquear carro en cola"
		  		+ " \nNo hay cupos disponibles o no hay carros en cola");
	  }
  }
 /**
  * M�todo para sacar un carro del parqueadero
  * @throws Exception
  */
  public void salidaCliente () throws Exception
  {
	  System.out.println("** REGISTRAR SALIDA CLIENTE **");
	  System.out.println("Ingrese la matr�cula del veh�culo");
	  String matricula = br.readLine();
	  
	  try{

		  double deuda = central.atenderClienteSale(matricula);
	  
		  System.out.println("El cliente de la matr�cula: "+matricula+
			  " Debe: " + deuda );
		  
		  System.out.println("**VUELVA PRONTO**");
	  }catch (Exception e){
		 throw new Exception("No se encontr� el veh�culo");
	  }
	  
  }
  /**
   * M�todo que permite visualizar graficaente el estado de los parqueaderos
   * @throws Exception
   */
  public void verEstadoParquederos() throws Exception
  {
	  System.out.println("** ESTADO PARQUEADEROS **");
	  Pila<Carro> copia1 = new Pila<Carro>();
	  Pila<Carro> copia2 = new Pila<Carro>();
	  Pila<Carro> copia3 = new Pila<Carro>();
	  Pila<Carro> copia4 = new Pila<Carro>();
	  Pila<Carro> copia5 = new Pila<Carro>();
	  Pila<Carro> copia6 = new Pila<Carro>();
	  Pila<Carro> copia7 = new Pila<Carro>();
	  Pila<Carro> copia8 = new Pila<Carro>();
	  copia1= central.darPila1().clone();
	  copia2= central.darPila2().clone();
	  copia3= central.darPila3().clone();
	  copia4= central.darPila4().clone();
	  copia5= central.darPila5().clone();
	  copia6= central.darPila6().clone();
	  copia7= central.darPila7().clone();
	  copia8= central.darPila8().clone();
	  System.out.println("PARQUEADERO 1" + "  PARQUEADERO 2  "+"PARQUEADERO 3" + "  PARQUEADERO 4  "+"PARQUEADERO 5" + "  PARQUEADERO 6  "+"PARQUEADERO 7" + "  PARQUEADERO 8  ");
	  for(int i=0;i<4;i++){
		  if(copia1.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia1.pop().darMatricula()+"       ");
		  }
		  if(copia2.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia2.pop().darMatricula()+"       ");
		  }
		  if(copia3.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia3.pop().darMatricula()+"       ");
		  }
		  if(copia4.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia4.pop().darMatricula()+"       ");
		  }
		  if(copia5.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia5.pop().darMatricula()+"       ");
		  }
		  if(copia6.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia6.pop().darMatricula()+"       ");
		  }
		  if(copia7.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia7.pop().darMatricula()+"       ");
		  }
		  if(copia8.getItemAtTop()!=null){
			  System.out.print("Carro: " + copia8.pop().darMatricula()+"       ");
		  }
		  System.out.println();
	  }
  }
  
  /**
   * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
   * @throws Exception
   */
  public void verCarrosEnCola () throws Exception
  {
	  System.out.println("** ESTADO COLA **");
	  Cola<Carro> pCola = new Cola<Carro>();
	  pCola=central.darCola().clone();
	  Carro item= null;

	  System.out.println("**DE PRIMERO A ULTIMO EN COLA**");
	  
		  while((item=pCola.dequeue())!=null){
			  
			  System.out.print("Carro: " + item.darMatricula() + " ");

		  }
	  
	  
	  System.out.println();
  }
  
  /**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
