package estructuras;

import java.util.Iterator;



public class Cola<T> implements Iterable<T>{
	private Node<T> primero;
	private Node<T> ultimo;
	private int size;
	
	public Cola(){
		this.primero=null;
		this.size=0;
	}
	public T dequeue(){
		T item=null;
		if(!isEmpty()){
			item=primero.getItem();
			primero=primero.getNext();
		}
		if(isEmpty()){
			ultimo = null;
		}
		return item;
	}
	
	public void enqueue(T item){
		Node<T> oldLast = this.ultimo;
		ultimo = new Node<T>(item, null);
		if(isEmpty()){
			primero = ultimo;
		}else{
			oldLast.setNext(ultimo);
		}
		size++;
	}
	public T getFirstItem(){
		return primero.getItem();
	}
	public T getLastItem(){
		return ultimo.getItem();
	}
	public boolean isEmpty() {
		 return primero == null;
	}
	
		public Iterator<T> iterator() { return new ListIterator();
		}
		
		private class ListIterator implements Iterator <T>{
			private Node<T> actual = primero;
			public boolean hasNext() {
				return actual!=null;
			}

			public T next() {
				T item = actual.getItem();
				actual = actual.getNext();
				return item;
			}
			
		}
	public Cola<T> clone(){
		Cola<T> copia = new Cola<T>();
		for(Iterator<T> it = this.iterator();it.hasNext();){
			copia.enqueue((T) it.next());
		}
		return copia;
	}
}
