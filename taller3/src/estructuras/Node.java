package estructuras;

public class Node<T> {
	
	private Node<T> next; 
	private T myItem;
	public Node(T item, Node<T> pNext) {

		next=pNext;
		myItem = item;
		
	}

	public T getItem() {
		
		return myItem;
	}

	public Node<T> getNext() {
		Node<T> item = null;
		if(next!=null){
			item = next;
		}
		return item;
	}
	
	public void setNext(Node<T> pNext){
		next = pNext;
	}
	
}
