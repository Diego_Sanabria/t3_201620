package estructuras;

import java.util.Iterator;





public class Pila <T> implements Iterable<T> {
	 private Node<T> top;
	 private int size;
	 public Pila(){
		 this.top=null;
		 this.size=0;
	 }
	 
	 public T getItemAtTop(){
		 if(isEmpty()){
			 return null;
		 }else{
			 return  top.getItem();
		 }
	 }
	 
	 public boolean isEmpty(){
		 
		 return top == null;
	 }
	 public int getSize(){
		 return size;
	 }
	 
	 public void push(T item){
		 top = new Node<T>(item,top);
		 size++;
		 
	 }
	 public T pop(){
		 
		 T item = null;
		 if(top!=null){
			 item = top.getItem();
			 top = top.getNext();
			 size --;
		 }
		 return item;
	 }

	public Iterator<T> iterator() { return new ListIterator();
	}
	
	private class ListIterator implements Iterator <T>{
		private Node<T> actual = top;
		public boolean hasNext() {
			return actual!=null;
		}

		public T next() {
			T item = actual.getItem();
			actual = actual.getNext();
			return item;
		}
		
	}
	public Pila<T> clone(){
		Pila<T> copia = new Pila<T>();
		for(Iterator<T> it = this.iterator();it.hasNext();){
			copia.push((T) it.next());
		}
		Pila<T> copia2=new Pila<T>();
		while(!copia.isEmpty()){
			copia2.push(copia.pop());
		}
		return copia2;
	}	 
	
	
}
