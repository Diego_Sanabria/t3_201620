package parqueadero;

import java.util.Date;
import java.util.Iterator;

import estructuras.Cola;
import estructuras.Pila;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	
	private Cola<Carro> carrosEnEspera;
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Pila<Carro> pila1;
	private Pila<Carro> pila2;
	private Pila<Carro> pila3;
	private Pila<Carro> pila4;
	private Pila<Carro> pila5;
	private Pila<Carro> pila6;
	private Pila<Carro> pila7;
	private Pila<Carro> pila8;
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	private Pila<Carro> pilaTemporal;
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
        carrosEnEspera= new Cola<Carro>();
        pilaTemporal = new Pila<Carro>();
        pila1 = new Pila<Carro>();
        pila2 = new Pila<Carro>();
        pila3 = new Pila<Carro>();
        pila4 = new Pila<Carro>();
        pila5 = new Pila<Carro>();
        pila6 = new Pila<Carro>();
        pila7 = new Pila<Carro>();
        pila8 = new Pila<Carro>();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro carro = new Carro(pColor, pMatricula, pNombreConductor);
    	carrosEnEspera.enqueue(carro);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	
    		Carro item = carrosEnEspera.dequeue();
    		String ubicacion = "";
    		
    		
    	if(item!=null){
    		ubicacion = parquearCarro(item);
    		return "Se ha parqueado el carro de matr�cula: "+ item.darMatricula() +" en la ubicaci�n: "+ ubicacion;
    		}else{
    		
    		throw new Exception ();	
    	
    	}
    	
    	
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {	
    	Carro item = sacarCarro(matricula);
    	if(item==null){
    		throw new Exception("No se encontr� el carro");
    	}else{
    	return cobrarTarifa(item);
    
    	}
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	
    	if(pila1.getSize()<4){
    		pila1.push(aParquear);

        	return "PARQUEADERO 1";    	    
    	}else if(pila2.getSize()<4){
    		pila2.push(aParquear);

        	return "PARQUEADERO 2";    	    
    	}else if(pila3.getSize()<4){
    		pila3.push(aParquear);

        	return "PARQUEADERO 3";    	    
    	}else if(pila4.getSize()<4){
    		pila4.push(aParquear);

        	return "PARQUEADERO 4";    	    
    	}else if(pila5.getSize()<4){
    		pila5.push(aParquear);

        	return "PARQUEADERO 5";    	    
    	}else if(pila6.getSize()<4){
    		pila6.push(aParquear);

        	return "PARQUEADERO 6";    	    
    	}else if(pila7.getSize()<4){
    		pila7.push(aParquear);

        	return "PARQUEADERO 7";    	    
    	}else if(pila8.getSize()<4){
    		pila8.push(aParquear);

        	return "PARQUEADERO 8";    	    
    	}else{
    		throw new Exception();
    	}
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	Carro item = null;   	
    	item=sacarCarroP1(matricula);
    	if(item==null){
    		item=sacarCarroP2(matricula);
    	}
    	if(item==null){
    		item=sacarCarroP3(matricula);
    	}
    	if(item==null){
    		item=sacarCarroP4(matricula);
    	}
    	if(item==null){
    		item=sacarCarroP5(matricula);
    	}
    	if(item==null){
    		item=sacarCarroP6(matricula);
    	}
    	if(item==null){
    		item=sacarCarroP7(matricula);
    	}
    	if(item==null){
    		item=sacarCarroP8(matricula);
    	}
    	
    	
    	return item;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila1.isEmpty()&& !encontrado){
    		if(pila1.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila1.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila1.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila1.pop());
    		}
    	}
    	return item;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila2.isEmpty()&& !encontrado){
    		if(pila2.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila2.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila2.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila2.pop());
    		}
    	}
    	return item;    
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila3.isEmpty()&& !encontrado){
    		if(pila3.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila3.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila3.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila3.pop());
    		}
    	}
    	return item;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila4.isEmpty()&& !encontrado){
    		if(pila4.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila4.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila4.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila4.pop());
    		}
    	}
    	return item;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila5.isEmpty()&& !encontrado){
    		if(pila5.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila5.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila5.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila5.pop());
    		}
    	}
    	return item;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila6.isEmpty()&& !encontrado){
    		if(pila6.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila6.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila6.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila6.pop());
    		}
    	}
    	return item;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila7.isEmpty()&& !encontrado){
    		if(pila7.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila7.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila7.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila7.pop());
    		}
    	}
    	return item;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	
    	Carro item = null;
    	boolean encontrado = false;
    	while(!pila8.isEmpty()&& !encontrado){
    		if(pila8.getItemAtTop().darMatricula().equals(matricula)){
    			item= pila8.pop();
    			while(!pilaTemporal.isEmpty()){
    				pila8.push(pilaTemporal.pop());
    			}
    			encontrado = true;
    		}else{
    			pilaTemporal.push(pila8.pop());
    		}
    	}
    	return item;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {	
    	Date fecha = new Date();
    	
    	return ((fecha.getTime()-car.darLlegada())/60000)*25;	
    }
    public Pila<Carro> darPila1(){
    	return pila1;
    }
    public Pila<Carro> darPila2(){
    	return pila2;
    }
    public Pila<Carro> darPila3(){
    	return pila3;
    }
    public Pila<Carro> darPila4(){
    	return pila4;
    }
    public Pila<Carro> darPila5(){
    	return pila5;
    }
    public Pila<Carro> darPila6(){
    	return pila6;
    }
    public Pila<Carro> darPila7(){
    	return pila7;
    }
    public Pila<Carro> darPila8(){
    	return pila8;
    }
    public Cola<Carro> darCola(){
    	return carrosEnEspera;
    }
}
